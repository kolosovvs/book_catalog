### Simple book catalog on Django REST framework and Vuejs
### Demo http://185.117.152.221:8080


### Deployment
#### Backend 

Setup environment
```
mkvirtualenv -p /usr/bin/python3.5 electronics_catalog
workon electronics_catalog
pip3 install -r requirements.txt
```

DB restore backup
```psql -h localhost -U book_catalog_user -d book_catalog -f book_catalog_dump.sql```


#### Frontend
Install nvm and npm
```
curl https://raw.githubusercontent.com/creationix/nvm/v0.24.1/install.sh | bash
nvm install v6.11.5
nvm use v6.11.5
```

Install packages
```
cd frontend
npm install
npm run dev
```
