from django.contrib import admin
from books.models import Book, Author, Comment, Publisher


class BookAdmin(admin.ModelAdmin):
    search_fields = ('title', )


class AuthorAdmin(admin.ModelAdmin):
    search_fields = ('first_name', 'last_name')


class CommentAdmin(admin.ModelAdmin):
    search_fields = ('comment', )

admin.site.register(Publisher)
admin.site.register(Comment, CommentAdmin)
admin.site.register(Book, BookAdmin)
admin.site.register(Author, AuthorAdmin)
