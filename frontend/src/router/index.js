import Vue from 'vue'
import Router from 'vue-router'
import About from '@/components/About'
import Books from '@/components/Books'
import BookDetail from '@/components/BookDetail'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/about',
      name: 'About',
      component: About
    },
    {
      path: '/',
      name: 'Books',
      component: Books
    },
    {
      path: '/:id',
      name: 'BookDetail',
      component: BookDetail
    },

  ]
})
