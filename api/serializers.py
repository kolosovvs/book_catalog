from rest_framework import serializers
from books.models import Book, Comment, Author, Publisher


class PublisherSerializer(serializers.ModelSerializer):

    class Meta:
        model = Publisher
        fields = '__all__'



class AuthorSerializer(serializers.ModelSerializer):

    class Meta:
        model = Author
        fields = '__all__'


class CommentSerializer(serializers.ModelSerializer):

    class Meta:
        model = Comment
        fields = ('name', 'email', 'comment')


class BookSerializer(serializers.ModelSerializer):
    comments = CommentSerializer(many=True)
    authors = AuthorSerializer(many=True)
    publisher = PublisherSerializer()

    class Meta:
        model = Book
        fields = ('authors', 'publisher', 'comments', 'id', 'title',
                  'description', 'pages', 'like', 'dislike', 'image')