from rest_framework.test import APITestCase
from books.models import Book
from django.core.management import call_command


class BooksTests(APITestCase):
    url = '/api/books/'

    def test_get_books(self):
        call_command('loaddata', 'books.json', verbosity=0)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(bool(response.data), True)
        self.assertEqual(Book.objects.count(), len(response.data))

    def test_get_one_book(self):
        call_command('loaddata', 'books.json', verbosity=0)
        response = self.client.get(self.url + '1/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(Book.objects.get(pk=1).title, response.data['title'])
